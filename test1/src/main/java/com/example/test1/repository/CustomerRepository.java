/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.repository;

import com.example.test1.model.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Personal
 */
//@Repository
public interface CustomerRepository extends JpaRepository <Customer, Long> {
    @Modifying
    @Query(value ="SELECT OrderTransaction.id, customer.name\n" +
"FROM OrderTransaction\n" +
"INNER JOIN customer ON OrderTransaction.id = customer.id;" ,nativeQuery = true)
    int deleteCustomer(Customer Customer); 
    
    @Query(value="select c from customer where c.name=:nama_customer")
    Customer findCustomerByName(@Param("namaCustomer")String nama);

//       
//      @Query(value ="SELECT * FROM Customer where id=?1", nativeQuery = true)
//        List<customer> findById(Long Id);
        
//      @Query(value ="SELECT * FROM Customer where name=?1 AND Id=?1", nativeQuery = true)
//        List<customer> findByNameandId(String name,Long Id);
//    public Customer findOne(Long id);
//   Long delete(Long id);

//    public Customer delete(Long Id);

}
