/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.repository;

import com.example.test1.model.OrderTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Personal
 */
public interface OrderRepository extends JpaRepository<OrderTransaction, Long > {
    
}
