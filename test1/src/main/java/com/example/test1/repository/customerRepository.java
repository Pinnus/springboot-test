/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.repository;

import com.example.test1.model.customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Personal
 */
//@Repository
public interface customerRepository extends JpaRepository <customer, Long> {
    @Modifying
    @Query("update customer cust set cust.customer = null where cust.customer =?1")
    int deleteCustomer(customer Customer);
    
//    public customer findOne(Long id);
//   Long delete(Long id);

//    public customer delete(Long Id);

}
