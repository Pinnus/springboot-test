/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Personal
 */
 @Entity
 @Table(name= "customer")
// @EntityListeners(AuditingEntityListener.class)
public class Customer {

//
    public Customer(Long Id, String Name, Integer Phone , String stock , String city) {
        this.Id = Id;
        this.Name = Name;
        this.Phone = Phone;
        this.stock = stock;
        this.city = city;
    }

    public Customer() {
    }

//    public Customer(String string) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

//    public Customer(Long orderId, Long id, String name, Integer phone, String stock, String city) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Integer getPhone() {
        return Phone;
    }

    public void setPhone(Integer Phone) {
        this.Phone = Phone;
    }
   
    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
     @Id
     @GeneratedValue(strategy=GenerationType.IDENTITY)
     private Long Id;

     @Column(name="name",nullable=false)
     private String Name;
     
     @Column(name="phone",nullable=false)
     private Integer Phone;
     
     
     @Column(name="stock",nullable=false)
     private String stock;

     @Column(name="city",nullable=false)
     private String city;
     
 
}
