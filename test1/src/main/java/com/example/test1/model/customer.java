/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Personal
 */
 @Entity
 @Table(name= "customer")
// @EntityListeners(AuditingEntityListener.class)
public class customer {

//
    public customer(Long Id, String Name, Integer Phone) {
        this.Id = Id;
        this.Name = Name;
        this.Phone = Phone;
    }

    public customer() {
    }

    public customer(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Integer getPhone() {
        return Phone;
    }

    public void setPhone(Integer Phone) {
        this.Phone = Phone;
    }
     @Id
     @GeneratedValue(strategy=GenerationType.AUTO)
     private Long Id;
     
     @Column(name="name",nullable=false)
     private String Name;
     
     @Column(name="phone",nullable=false)
     private Integer Phone;
   
    
}
