/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Personal
 */
@Entity
@Table(name= "ordertransaction")
public class OrderTransaction {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="order_name",nullable = false)
    private String OrderName;
    
     @ManyToOne
     @JoinColumn(name = "Customer_Id", referencedColumnName = "id", updatable = true,nullable = false)
     private Customer customer;
     
    
    public OrderTransaction(Long id, String OrderName) {
        this.id = id;
        this.OrderName = OrderName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderName() {
        return OrderName;
    }

    public void setOrderName(String OrderName) {
        this.OrderName = OrderName;
    }

    
}
