/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.massages;

/**
 *
 * @author Personal
 */
public class GeneralMassage {
    private boolean status;
    private String errorMassage;
    private String successMassage;

    public GeneralMassage(boolean status, String errorMassage, String successMassage) {
        this.status = status;
        this.errorMassage = errorMassage;
        this.successMassage = successMassage;
    }

    public GeneralMassage() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getErrorMassage() {
        return errorMassage;
    }

    public void setErrorMassage(String errorMassage) {
        this.errorMassage = errorMassage;
    }

    public String getSuccessMassage() {
        return successMassage;
    }

    public void setSuccessMassage(String successMassage) {
        this.successMassage = successMassage;
    }
    
    
}
