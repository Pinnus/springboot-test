package com.example.test1;

import com.example.test1.model.Customer;
//import customerService.customerService;
import java.util.List;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Test1Application {
    
//    customerService CustomerService;

	public static void main(String[] args) {
		SpringApplication.run(Test1Application.class, args);
	}
//        
//        public void run(String... args) throws Exception{
//            getCustomerInfoByNameAndId();
//        }
//       public void getCustomerInfoByNameAndId(){
//           List <customer> customerList = CustomerService.getCustomerInfoByNameAndId("pinnn", 0L);
//           customerList.forEach(System.out::println);
//       }
//       

}
