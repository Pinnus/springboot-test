/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.controller;

import com.example.test1.constructorWrapper.ConstructorWrapper;
import com.example.test1.massages.GeneralMassage;
import com.example.test1.model.Customer;
import com.example.test1.model.OrderTransaction;
//import com.example.test1.customerdata.customerData;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.test1.repository.CustomerRepository;
import com.example.test1.model.OrderTransaction;

/**
 *
 * @author Personal
 */

@RestController
@RequestMapping("company")
public class CustomerController {
    
    @Autowired
    CustomerRepository customerRepository; 
   // customerData CustomerData;
      
    
    //save an Customer
 //   @PostMapping("/customer")
//    public Customer createCustomer(@Valid @RequestBody Customer cust){
//        return Customer;
//    }
    
    //cobacoba
    @RequestMapping(value = "/add", method = RequestMethod.POST,consumes = 
            "application/json",produces = "application/json")
    public @ResponseBody Customer addCustomer(@RequestBody ConstructorWrapper consw ){
                Customer cust = new Customer(consw.getId(),consw.getName(),consw.getPhone(),consw.getStock(),consw.getCity());
                Customer custHasilAdd = customerRepository.save(cust);
                return custHasilAdd;
            }
          //  @RequestBody Customer cust , 
       
//    {
//        cust.setId(consw.getId());
//        cust.setName(consw.getName());
//        cust.setPhone(consw.getPhone());
//        cust.setStock(consw.getStock());
//        cust.setCity(consw.getCity());
//        Customer hasilCust = CustomerRepository.save(cust);
//        return hasilCust;
//    }
    //ga dipake dolo
   
    //get all Customer
    
    @GetMapping("/customer")
    public List <Customer> getAllCustomer(){
        return customerRepository.findAll();
        
    }
    
    //get Customer by custid
    @GetMapping("/notes/id")
    public ResponseEntity <Customer> getCustomerById(@PathVariable(value="id")
    Long custid){
        
        Customer cust = customerRepository.getOne(custid);
        
        if(cust==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(cust);
    }
    
//     @GetMapping("/hello")
//    public String sayHello() {
//        return "Hello Fucker!";
//    }
//    
//    //update Customer by id
//    
//    @PostMapping("/update")
//    public ResponseEntity <customer> updateCustomerById(@PathVariable(value="id")
//    Long custid, @Valid @RequestBody Customer custDetails){
//        
//        Customer cust = CustomerRepository.getOne(custid);
//       
//        if(cust==null){
//            return ResponseEntity.notFound().build();
//            
//        }
//        cust.setId(custDetails.getId());
//        cust.setName(custDetails.getName());
//        cust.setPhone(custDetails.getPhone());
//        
//        Customer updateCustomer = CustomerRepository.save(cust);
//        return ResponseEntity.ok().body(updateCustomer);
//    }
    @RequestMapping(value= "/update", method =RequestMethod.POST, consumes 
            = "application/json" , produces = "application/json")
    public @ResponseBody Customer updateCustomerById(@RequestBody 
            ConstructorWrapper consw){ 
   //     try{
   Customer cust = new Customer(consw.getId(),consw.getName(),consw.getPhone(),consw.getStock(),consw.getCity());
//        Customer cust = CustomerRepository.getOne(consw.getId());
//        OrderTransaction ord = CustomerRepository.getOne(consw.getOrderId());
                //findById(consw.getId());
        
       
        
        Customer updateCustomer = customerRepository.save(cust);
        
        return updateCustomer;
        
            
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//       
       
    }
    
    //Delete Customer
//    @DeleteMapping("/delete")
//    
//    public ResponseEntity <customer> deleteCustomer(@PathVariable(value="id")
//    Long custid){
//        Customer cust = (Customer) CustomerRepository.getOne(custid);
//        if(cust==null){
//            return ResponseEntity.notFound().build();
//        }
//        CustomerRepository.delete(cust);
//     
//        return ResponseEntity.ok().build();
//    }
 
//    @PostMapping("/customers")
//    public String Customer(){
//        
//        return ("Customer");
//    
//    
//} 
    
 @RequestMapping(value= "/delete", method =RequestMethod.GET, produces = 
         "application/json")
    public @ResponseBody GeneralMassage deleteById(@RequestParam (name="Id",required =
            true)Long Id){ 
//        Customer cust = CustomerRepository.getOne(Id);
        
        customerRepository.deleteById((Id));

         return new GeneralMassage(true, "", "berhasil ngedelete, idnya:"+Id);
    }
    
}
