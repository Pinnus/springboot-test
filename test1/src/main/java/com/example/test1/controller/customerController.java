/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.controller;

import com.example.test1.constructorWrapper.constructorWrapper;
import com.example.test1.massages.GeneralMassage;
import com.example.test1.model.customer;
import com.example.test1.repository.customerRepository;
//import com.example.test1.customerdata.customerData;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Personal
 */

@RestController
@RequestMapping("company")
public class customerController {
    
    @Autowired
    customerRepository CustomerRepository; 
   // customerData CustomerData;
      
    
    //save an customer
 //   @PostMapping("/customer")
//    public customer createCustomer(@Valid @RequestBody customer cust){
//        return Customer;
//    }
    
    //cobacoba
    @RequestMapping(value = "/add", method = RequestMethod.POST,consumes = 
            "application/json",produces = "application/json")
    public @ResponseBody customer addCustomer(@RequestBody customer cust)
    {
        customer hasilCust = CustomerRepository.save(cust);
        return hasilCust;
    }
    //ga dipake dolo
   
//    //get all customer
//    
//    @GetMapping("/customer")
//    public List <customer> getAllCustomer(){
//        return CustomerRepository.findAll();
//        
//    }
//    
//    //get customer by custid
//    @GetMapping("/notes/{id}")
//    public ResponseEntity <customer> getCustomerById(@PathVariable(value="id")
//    Long custid){
//        
//        customer cust = CustomerData.findOne(custid);
//        
//        if(cust==null){
//            return ResponseEntity.notFound().build();
//        }
//        return ResponseEntity.ok().body(cust);
//    }
    
//     @GetMapping("/hello")
//    public String sayHello() {
//        return "Hello Fucker!";
//    }
//    
    //update customer by id
    
//    @PostMapping("/update")
//    public ResponseEntity <customer> updateCustomerById(@PathVariable(value="id")
//    Long custid, @Valid @RequestBody customer custDetails){
//        
//        customer cust = CustomerRepository.getOne(custid);
//       
//        if(cust==null){
//            return ResponseEntity.notFound().build();
//            
//        }
//        cust.setId(custDetails.getId());
//        cust.setName(custDetails.getName());
//        cust.setPhone(custDetails.getPhone());
//        
//        customer updateCustomer = CustomerRepository.save(cust);
//        return ResponseEntity.ok().body(updateCustomer);
//    }
    @RequestMapping(value= "/update", method =RequestMethod.POST, consumes 
            = "application/json" , produces = "application/json")
    public @ResponseBody customer updateCustomerById(@RequestBody 
            constructorWrapper consw, Long custid){ 
   //     try{
        customer cust = CustomerRepository.getOne(consw.getId());
                //findById(consw.getId());
        
        cust.setId(consw.getId());
        cust.setName(consw.getName());
        cust.setPhone(consw.getPhone());
        
        customer updateCustomer = CustomerRepository.save(cust);
        
        return updateCustomer;
        
            
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//       
       
    }
    
    //Delete customer
//    @DeleteMapping("/delete")
//    
//    public ResponseEntity <customer> deleteCustomer(@PathVariable(value="id")
//    Long custid){
//        customer cust = (customer) CustomerRepository.getOne(custid);
//        if(cust==null){
//            return ResponseEntity.notFound().build();
//        }
//        CustomerRepository.delete(cust);
//     
//        return ResponseEntity.ok().build();
//    }
 
//    @PostMapping("/customers")
//    public String customer(){
//        
//        return ("customer");
//    
//    
//} 
    
 @RequestMapping(value= "/delete", method =RequestMethod.GET, produces = 
         "application/json")
    public @ResponseBody GeneralMassage delete(@RequestParam (name="Id",required =
            true)Long id){ 
//        customer cust = CustomerRepository.getOne(Id);
        
        CustomerRepository.delete(CustomerRepository.getOne(id));

         return new GeneralMassage(true, "", "berhasil ngedelete, idnya:"+id);
    }
    
}
