/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.controller;

import com.example.test1.constructorWrapper.ConstructorWrapper;
import com.example.test1.constructorWrapper.ConstructorWrapperOrder;
import com.example.test1.massages.GeneralMassage;
import com.example.test1.model.Customer;
import com.example.test1.model.OrderTransaction;
import com.example.test1.repository.CustomerRepository;
import com.example.test1.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Personal
 */
@RestController
@RequestMapping("order")

public class OrderController {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CustomerRepository  customerRepository;
    
    @RequestMapping(value = "/add/order", method = RequestMethod.POST,consumes = 
            "application/json",produces = "application/json")
    public @ResponseBody OrderTransaction addOrder(@RequestBody ConstructorWrapperOrder conswo ){
                Customer cust = customerRepository.findCustomerByName(conswo.getNamaCustomer());
                OrderTransaction ord = new OrderTransaction(conswo.getId(),conswo.getOrderName());
                OrderTransaction hasilOr = orderRepository.save(ord);
                
               
                
                return hasilOr;
            }
    
     @RequestMapping(value= "/update/orderid", method =RequestMethod.POST, consumes 
            = "application/json" , produces = "application/json")
    public @ResponseBody OrderTransaction updOrderTransaction(@RequestBody ConstructorWrapperOrder conswo ){
        OrderTransaction ord= new OrderTransaction(conswo.getId(), conswo.getOrderName());
        OrderTransaction updateOrder = orderRepository.save(ord);
        
        return updateOrder;
    }
    @RequestMapping(value= "/delete/order", method =RequestMethod.GET, produces = 
         "application/json")
    public @ResponseBody GeneralMassage deleteById(@RequestParam (name="Id",required =
            true)Long Id){ 
//        Customer cust = CustomerRepository.getOne(Id);
        
        orderRepository.deleteById((Id));

         return new GeneralMassage(true, "", "berhasil ngedelete, idnya:"+Id);
    }
}
