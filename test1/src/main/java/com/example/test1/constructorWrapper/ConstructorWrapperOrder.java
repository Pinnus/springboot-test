/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test1.constructorWrapper;

/**
 *
 * @author Personal
 */
public class ConstructorWrapperOrder {
    private Long id;
    private String OrderName;
    private String namaCustomer;

    public ConstructorWrapperOrder() {
    }

    public ConstructorWrapperOrder(Long id,String OrderName , String namaCustomer) {
        this.id = id;
        this.OrderName = OrderName;
        this.namaCustomer = namaCustomer;
    }

    public Long getId() {
        return id;
    }

    public String getNamaCustomer() {
        return namaCustomer;
    }

    public void setNamaCustomer(String namaCustomer) {
        this.namaCustomer = namaCustomer;
    }

    public String getOrderName() {
        return OrderName;
    }

    public void setOrderName(String OrderName) {
        this.OrderName = OrderName;
    }

    public void setId(Long id) {
        this.id = id;
    }


    
}
