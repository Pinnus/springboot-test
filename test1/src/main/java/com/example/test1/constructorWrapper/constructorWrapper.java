package com.example.test1.constructorWrapper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package constructorWrapper;
//
///**
// *
// * @author Personal
// */
public class constructorWrapper {
      private Long Id;
    private String Name;
    private Integer Phone;

    public constructorWrapper(Long Id, String Name, Integer Phone) {
        this.Id = Id;
        this.Name = Name;
        this.Phone = Phone;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Integer getPhone() {
        return Phone;
    }

    public void setPhone(Integer Phone) {
        this.Phone = Phone;
    }
    
}
