package com.example.test1.constructorWrapper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package ConstructorWrapper;
//
///**
// *
// * @author Personal
// */
public class ConstructorWrapper {
    private Long Id;
    private String Name;
    private Integer Phone;
    private String stock;
    private String city;

    

    public ConstructorWrapper(Long Id, String Name, Integer Phone , String stock,
            String city) {
        
        this.Id = Id;
        this.Name = Name;
        this.Phone = Phone;
        this.stock= stock;
        this.city= city;
    }
    

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Integer getPhone() {
        return Phone;
    }

    public void setPhone(Integer Phone) {
        this.Phone = Phone;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    
}
